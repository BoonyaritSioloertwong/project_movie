<?php

/*
--------------------------------------------------------------------------
 Application Routes
--------------------------------------------------------------------------

 Here is where you can register all of the routes for an application.
 It is a breeze. Simply tell Lumen the URIs it should respond to
 and give it the Closure to call when that URI is requested.
*/

use Firebase\JWT\JWT ; 
 
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/list_movie_event',['middleware' => 'auth',function() {
	
	$results = app('db')->select("SELECT * FROM TheMovies");
	
	return response()->json($results);
}]);

$router->post('add_event',function(Illuminate\Http\Request $request) {
	$event_name = $request->input("event_name");
	$event_datetime = strtotime ($request->input("event_datetime"));
	$event_datetime = date ('Y-m-d H:i:s',$event_datetime);
    $event_theatre =  $request->input("event_theatre");
	$event_seatno = $request->input("event_seatno");
	$query = app('db')->insert('INSERT into TheMovies
	                           (EventName ,EventDate,Theatre,Seatno)
							   VALUES(?,?,?,?)',
							   [ $event_name,
							     $event_datetime,
								 $event_theatre,
								 $event_seatno ] );
			return "Ok";
});
$router->put('update_movie_event',function(Illuminate\Http\Request $request) {
	$event_id_movie = $request->input("event_id_movie");
	$event_name = $request->input("event_name");
	$event_datetime = strtotime ($request->input("event_datetime"));
	$event_datetime = date ('Y-m-d H:i:s',$event_datetime);
	$event_theatre =  $request->input("event_theatre");
	$event_seatno = $request->input("event_seatno");
	
	$query = app('db')->update('UPDATE TheMovies
	                           SET EventName=?,
									EventDate=?,
									Theatre=?,
									SeatNo=?
							WHERE
								EventID_movie = ?',
								[ $event_name,
								  $event_datetime,
								  $event_theatre,
								  $event_seatno,
								  $event_id_movie] );
								  
			return "Ok";
});

$router->delete('delete_movie_event',function (Illuminate\Http\Request $request) {
		$event_id_movie = $request->input("event_id_movie");
		$query = app('db')->delete('DELETE FROM TheMovies
										WHERE
												EventID_movie=?',
												[ $event_id_movie] );
												
	return "Ok";

});

$router->post('/register_movie',['middleware'=>'auth',function(Illuminate\Http\Request  $request) {
	$event_id = $request->input("event_id");
	
	
	$movie_cinema = $request->input("movie_cinema");
	$movie_seat = $request->input("movie_seat");
	$user = app('auth')->user();
	$userID = $user->id;
	
	
	
	$query = app('db')->insert('INSERT into ThemovieRegistration
								(EventID,UserID,Cinema,Seat,ConfirmRegister)
								VALUES (?,?,?,?,?)',
								[ $event_id,
								  $userID,
								  $movie_cinema,
								  $movie_seat,
								  
								  'n' ] );
				return"Ok";
}]);	
		
$router->get('list_movie',function(){
 $results = app('db')->select("SELECT  RegID,
										user.Name,
										user.Surname,
										Cinema,
										Seat,
									    TheMovies.EventName,
										ConfirmRegister
										FROM TheMovies,ThemovieRegistration,user
										WHERE (TheMovies.EventID_movie = ThemovieRegistration.EventID) AND
										(user.UserID = ThemovieRegistration.UserID)
										");
										
					return response() -> json($results);
});	
	


$router->post('/register',function(Illuminate\Http\Request $request) {
			  $name = $request->input("name");
			  $surname = $request->input("surname");
			  $email = $request->input("email");
			  $username = $request->input("username");
			  $password = app('hash')->make($request->input("password"));
			  
			  $query = app('db')->insert('INSERT into User
									(Name,Surname,Email,Username,Password,Admin)
									VALUES(?,?,?,?,?,?)',
									[ $name,
									  $surname,
									  $email,
									  $username,
									  $password,
									  'n', ] );
						return "Ok";
});						

$router->post ('/login',function (Illuminate\Http\Request  $request) {

$username = $request->input("username");
$password = $request->input("password");

$result = app('db')->select('SELECT UserID,password from user WHERE Username =?',
							[$username]);
							
		$loginResult = new stdClass();
		
		if(count($result) == 'n') {
			$loginResult->status = "fail";
			$loginResult->reason= "User is not founded";
		}else{
			if(app('hash')->check($password,$result[0]->password)) {
				$loginResult->status = "success";
				
				$payload = [
				'iss' =>"themovie_system",
				'sub' => $result[0]->UserID,
				'iat' =>time(),
				'exp' =>time() +30 *60 *60, 
			];
			$loginResult->token = JWT ::encode($payload,env('APP_KEY'));
			
			return response()->json($loginResult);
			
			}else {
				$loginResult->status = "fail";
				$loginResult->reason= "Incorrect Password";
				return response()->json($loginResult);
				
			}
		}
		return response()->json($loginResult);
		
});

$router->put('/confirm_register',['middleware' =>'auth',function (Illuminate\Http\Request $request) {
	$reg_id = $request->input("reg_id");
	$user = app('auth')->user();
	$userID = $user->id;
	//check wheter if users is an admin or not
	$result = app('db')->select("SELECT admin FROM user WHERE UserID = ?", [$userID]);
		if($result [0]->admin == 'y'){
			//User is an admin ,allow him to switch payment info
			$query =app('db')->insert('UPDATE ThemovieRegistration
								SET confirmregister = "Confirm"
								WHERE
										RegID =?',
										[ $reg_id ]);
				return "Ok";
		}else{
					return "Unauthorized, only admin is allowed";
		}
}]);	
