-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for themovie
CREATE DATABASE IF NOT EXISTS `themovie` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `themovie`;

-- Dumping structure for table themovie.themovieregistration
CREATE TABLE IF NOT EXISTS `themovieregistration` (
  `RegID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EventID` int(10) unsigned NOT NULL,
  `UserID` varchar(50) NOT NULL DEFAULT '',
  `Cinema` varchar(50) NOT NULL DEFAULT '',
  `Seat` varchar(50) NOT NULL DEFAULT '',
  `ConfirmRegister` varchar(50) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`RegID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table themovie.themovies
CREATE TABLE IF NOT EXISTS `themovies` (
  `EventID_movie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EventName` varchar(50) NOT NULL DEFAULT '',
  `EventDate` datetime DEFAULT NULL,
  `Theatre` varchar(50) NOT NULL DEFAULT '',
  `Seatno` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`EventID_movie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table themovie.user
CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(80) NOT NULL,
  `Admin` char(50) DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
